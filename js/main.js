$(document).ready(function() {

  function Tile(row, column) {
    this.row = row;
    this.column = column;
    this.cls = 'cell empty';
  }

  function Board()  {
    this.grid = [];
  }

  Board.prototype.buildGrid = function() {
    for( var r = 0; r < 20; r++ ) {
      for( var c = 0; c < 20; c++ ) {
        tile = new Tile(r, c);
        this.grid.push(tile);
      }
    }
  };

  Board.prototype.renderGrid = function(wrapper) {
    for ( var idx in this.grid ) {
      tile = this.grid[idx];
      $tile = $('<div></div>');
      $tile.addClass(tile.cls);
      $tile.attr('id', 'c_' + tile.row + '_' + tile.column);
      wrapper.append($tile);
    }

  };


  function Food() {
    this.row = 10;
    this.column = 10;
  }

  Food.prototype.generate = function() {
    r = Math.floor(Math.random() * 20);
    c = Math.floor(Math.random() * 20);
    this.row = r;
    this.column = c;
  };


  function Snake() {
    this.row = 5;
    this.column = 5;
    this.speed = 200;
    this.body = [[5,5]];
  }

  Snake.prototype = {
    constructor: Snake,
    slower: function() {
      this.speed *= 1.5;
    },
    faster: function() {
      this.speed *= 0.98;
    },
    grow: function() {
      this.body.unshift([this.row, this.column]);
    },
    move: function(direction) {
      switch(direction) {
        case 'down':
          this.row++;
          this.grow();
          break;
        case 'up':
          this.row--;
          this.grow();
          break;
        case 'left':
          this.column--;
          this.grow();
          break;
        case 'right':
          this.column++;
          this.grow();
          break;
        default:
          console.log('use arrow keys');
        }
      }
  };

  function Game() {
    this.score = 0;
    this.gameOver = false;
    this.pause = true;
    this.direction = 40;

    this.render = function render() {
      $(".cell").remove();
      $wrapper = $wrapper = $(".wrapper");
      board = new Board();
      board.buildGrid();
      board.renderGrid($wrapper);
      snake = new Snake();
      food = new Food();
      food.generate();

      $("#c_"+snake.row+"_"+snake.column).addClass("snake");
      $("#c_"+food.row+"_"+food.column).addClass("food");
      $(".instructions").text("Press 's' to start. Press 'p' to pause. Use arrow keys to change direction.");
      $("#counter").text(this.score);
    };
  }

  //TODO clean up

  Game.prototype.update = function() {
    switch(this.direction) {
        case 40:
          snake.move('down');
        break;
        case 39:
          snake.move('right');
        break;
        case 38:
          snake.move('up');
        break;
        case 37:
          snake.move('left');
        break;
        default:
          console.log('Use arrow keys');
    }

    if (snake.row < 0 || snake.row > 19 || snake.column < 0 || snake.column > 19 ||
      $("#c_"+snake.row+"_"+snake.column).hasClass("snake") ) {
        this.gameOver = true;
        $(".cell").addClass("gameOver");
        if (game.score > highScore) {
            highScore = game.score;
            docCookies.setItem('snake-high-score', highScore.toString());
        }
        $(".instructions").text("Game Over! Press 's' to restart.");
    }

    if( snake.row === food.row && snake.column === food.column ) {
      $("#c_"+snake.row+"_"+snake.column).addClass("snake");
      $("#c_"+snake.row+"_"+snake.column).removeClass("food");
      this.score++;
      if (this.score % 5 === 0) {
        snake.faster();
      }
      $("#counter").text(this.score);
      food.generate();
      while( $("#c_"+food.row+"_"+food.column).hasClass("snake") ) {
        food.generate();
      }
      $("#c_"+food.row+"_"+food.column).addClass("food");
    } else {
      $("#c_"+snake.row+"_"+snake.column).addClass("snake");
      tail = snake.body.pop();
      $("#c_"+tail[0]+"_"+tail[1]).removeClass("snake");
    }
  };

  Game.prototype.next_move = function() {
    var _this = this;
    setTimeout(function() {
      // split into render and move functions
      _this.update();
      if( !_this.gameOver && !_this.pause) {
        _this.next_move();
      }
    }, snake.speed);
  };

  // start game
  var highScore = 0;
  var getCookie = function() {
    var cookie = docCookies.getItem('snake-high-score');
    if (cookie) {
        highScore = parseInt(cookie);
        $("#high-score").text(highScore);
    }
  };

  var game = new Game();
  getCookie();
  game.render();

 // TODO clean up
  $(document).keydown(function(e) {
      var arrow_keys = [40,39,38,37];
      var key_code = e.which;
      if( arrow_keys.indexOf(key_code) >= 0 ) {
        game.direction = e.which;
      } else if ( key_code === 80) {
          game.pause = !game.pause;
          if( !game.pause ) {
            game.next_move();
          }
      } else if ( key_code === 83 ) {
        if( game.pause ) {
          game.pause = !game.pause;
          game.next_move();
        } else if( game.gameOver ) {
          $("#previous-score").text(game.score);
          getCookie();
          game = new Game();
          game.render();
        }
      } else if ( key_code === 70 ) {
          snake.faster();
      } else if( key_code === 82 ) {
          snake.slower();
      }
  });
});
